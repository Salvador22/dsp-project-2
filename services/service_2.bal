import ballerinax/kafka;
import ballerina/log;
import ballerina/time;
import ballerina/file;
import ballerina/io;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "2",
    topics: ["reply"],

    pollingInterval: 1,
    autoCommit: false
};

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

listener kafka:Listener kafkaListener =
        new (kafka:DEFAULT_URL, consumerConfigs);

service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,
                                kafka:ConsumerRecord[] records) returns error? {
        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }

        kafka:Error? commitResult = caller->commit();

        if commitResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    byte[] value = kafkaRecord.value;

    string messageContent = check string:fromBytes(value);
    io:StringReader sr = new (messageContent, encoding = "UTF-8");
    //converts the message back to a json for reading and storing
    json j = check sr.readJson();

    // Create a record of type ‘Time’.
    time:Utc currentUtc = time:utcNow();
    string utcString = time:utcToString(currentUtc);
    //Sets file name
    string getfileName = check j.key;
    string opera = check j.opr;
    json _to_save = {_key: check j.key, data: check j.data, _time: utcString};
    //prints out the json before storing 
    io:println("Received Message: ", _to_save);

    //sends the data and file name for storing

    io:println("Received Message: ", j.opr);
    if (opera == "wr") {
        error? storeResult = store(_to_save, getfileName);
    }
    if (opera == "r") {
        error? storeResult = read(getfileName);
    }

}

public function send(json msgs) returns error? {
    string message = msgs.toJsonString();
    io:println(message);
    check kafkaProducer->send({
        topic: "test",
        value: message.toBytes()
    });
    check kafkaProducer->'flush();

}

function read(string fname) returns error? {
    string finame = fname + ".json";
    string jsonFilePath = "./files/" + finame;
    //checks if the file exists
    json readJson1 = null;
    error|boolean fileExists = file:test(jsonFilePath, file:EXISTS);
    if fileExists == true {
        error|json readJson = io:fileReadJson(jsonFilePath);
        readJson1 = check readJson.ensureType();
        json rspData = {operation: "r", _data: readJson1, message: "file found!!"};
        error? sendResult = send(rspData);
    } else if fileExists == false {
        json rspData = {operation: "r", _data: {}, message: "file not found!!"};
        error? sendResult = send(rspData);
    }
}

function store(json data, string fname) returns error? {
    string finame = fname + ".json";
    string jsonFilePath = "./files/" + finame;
    json jsonContent = data;
    //checks if the file exists
    json readJson1 = null;
    error|boolean fileExists = file:test(jsonFilePath, file:EXISTS);

    if fileExists == true {
        error|json readJson = io:fileReadJson(jsonFilePath);
        readJson1 = check readJson;
        io:Error? fileWriteJson = io:fileWriteJson(jsonFilePath, jsonContent);
        error|json readJson2 = io:fileReadJson(jsonFilePath);

        json rspData = {operation: "owr", old_data: readJson1, new_data: check readJson2, message: "file stored s2"};
        error? sendResult = send(rspData);

    } else if fileExists == false {
        io:Error? fileWriteJson = io:fileWriteJson(jsonFilePath, jsonContent);
        error|json readJson2 = io:fileReadJson(jsonFilePath);
        error|boolean fileExists1 = file:test(jsonFilePath, file:EXISTS);
        //chechs fro new file
        if fileExists1 == true {
            json rspData = {operation: "wr", data: check readJson2, message: "file stored s2"};
            error? sendResult = send(rspData);
        }
    }

}
