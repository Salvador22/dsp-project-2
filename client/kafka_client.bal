import ballerinax/kafka;
import ballerina/io;

kafka:ConsumerConfiguration consumerConfiguration = {
    groupId: "1",
    offsetReset: "earliest",
    topics: ["test"]

};

kafka:Consumer consumer = check new (kafka:DEFAULT_URL, consumerConfiguration);

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    //json a = {opr: "wr", key: "1", data: {name: "kamo", age: "12"}};
    //json b = {opr: "r", key: "1"};
    json d = {opr: "wr", key: "1", data: {name: "roro", age: "24"}};
    //json c = {opr: "r", key: "2"};
    error? recieveResult = write(d);
   // error? Result = read(b);

}

public function recieve() returns error? {
    kafka:ConsumerRecord[] records = check consumer->poll(1);

    int a = records.length();
    int i = 0;
    foreach var kafkaRecord in records {
        if (i == (a - 1)) {
            byte[] messageContent = kafkaRecord.value;
            string message = check string:fromBytes(messageContent);

            io:println("Received Message: " + message);
        }

        i = i + 1;
    }

}

public function send() returns error? {
   

    json a = {opr: "r", key: "3", data: {}};
    io:println("Send from client:", a);
    string s = a.toJsonString();
    check kafkaProducer->send({
        topic: "reply",
        value: s.toBytes()
    });

    check kafkaProducer->'flush();
    error? recieveResult1 = recieve();
}

function write(json data) returns error? {

    io:println("Send from client:", data);
    string s = data.toJsonString();
    check kafkaProducer->send({
        topic: "reply",
        value: s.toBytes()
    });

    check kafkaProducer->'flush();
    error? recieveResult1 = recieve();
}

function read(json data) returns error? {
    io:println("Send from client:", data);
    string s = data.toJsonString();
    check kafkaProducer->send({
        topic: "reply",
        value: s.toBytes()
    });

    check kafkaProducer->'flush();
    error? recieveResult1 = recieve();
}

